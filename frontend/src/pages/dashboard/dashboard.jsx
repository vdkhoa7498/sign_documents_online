import React, { useEffect } from 'react';
import { Row, Col } from 'antd';
import SideNav from '../../components/sideNav/sideNav';
import './dashboard.scss';
import HeaderNav from '../../components/headerNav/headerNav';
import TableDocument from '../../components/tableDocument/tableDocument';
import Footer from '../../components/footer/footer';
export default function Dashboard() {
  return (
    <div className="dashboard-page">
      <HeaderNav />
      <Row>
        <Col span={5}>
          <SideNav />
        </Col>
        <Col span={19}>
          <TableDocument title="SENT" />
        </Col>
      </Row>

      <Footer />
    </div>
  );
}

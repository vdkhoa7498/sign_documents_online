import React, { useEffect, useState } from 'react';
import { Row, Col, Steps, Button, message as antMessage } from 'antd';
import { useLocation, useParams, useHistory } from 'react-router-dom';
import { Prompt } from 'react-router';

import SideNav from '../../components/sideNav/sideNav';
import HeaderNav from '../../components/headerNav/headerNav';
import Footer from '../../components/footer/footer';
import AddDocument from '../../components/addDocument/addDocument';
import AddRecipients from '../../components/addRecipients/addRecipients';
import Recipient from '../../components/recipient/Recipient';
import ReviewDocument from '../../components/reviewDocument/reviewDocument';

import { getEnvelopeById } from '../../services/envelope';

export default function PrepareEnvelope() {
  const location = useLocation();
  const params = useParams();
  const history = useHistory();
  const { Step } = Steps;
  const [current, setCurrent] = useState(0);
  const [envelope, setEnvelope] = useState(null);
  const { envelopeId } = params;

  // const checkUpload = (file, setCurrent) => {
  //   if (!file) {
  //     setCurrent(0);
  //     antMessage.error('You need to upload 1 document first!');
  //   } else {
  //     setCurrent(1);
  //   }
  // };

  useEffect(() => {
    async function doStuff() {
      try {
        const { success, envelope, message } = await getEnvelopeById(
          envelopeId
        );
        if (success) {
          setEnvelope(envelope);
          const url = location.pathname.split('/')[3];
          if (url === 'add-document') {
            setCurrent(0);
          } else if (url === 'add-recipients') {
            if (!envelope.file) {
              history.push(`/prepare/${envelope.envelopeId}/add-document`);
              antMessage.error('You need to upload 1 document first!');
              setCurrent(0);
            } else {
              setCurrent(1);
            }
            return;
          } else if (url === 'finish-and-send') {
            if (!envelope.file) {
              history.push(`/prepare/${envelope.envelopeId}/add-document`);
              setCurrent(0);
              antMessage.error('You need to upload 1 document first!');
              return;
            } else if (envelope.recipients.length === 0) {
              history.push(`/prepare/${envelope.envelopeId}/add-recipients`);
              setCurrent(1);
              antMessage.error('You need to add recipients first!');
              return;
            } else {
              console.log('Here');
              setCurrent(2);
            }
          }
        } else {
          console.log(message);
          // antMessage.error(message);
          history.push('/');
        }
      } catch (errorMessage) {
        console.log(errorMessage);
        // antMessage.error(errorMessage);
      }
    }
    doStuff();
  }, [envelopeId]);

  const next = () => {
    setCurrent(current + 1);
  };

  const prev = () => {
    setCurrent(current - 1);
  };

  const steps = [
    {
      title: 'Add Document',
      content: <AddDocument envelope={envelope} onNext={() => next()} />,
    },
    {
      title: 'Add Recipients',
      content: (
        <Recipient
          envelope={envelope}
          onPrev={() => prev()}
          onNext={() => next()}
        />
      ),
    },
    {
      title: 'Review & Send',
      content: <ReviewDocument envelope={envelope} onClick={() => next()} />,
    },
  ];
  return (
    <div>
      <React.Fragment>
        {/* <Prompt message="You have unsaved changes, are you sure you want to leave?" /> */}
        {/* Component JSX */}

        <HeaderNav />
        <Row>
          <Col span={18} style={{ marginRight: 50 }}>
            <Steps
              current={current}
              style={{ marginLeft: '15%', marginTop: '2%' }}
            >
              {steps.map((item) => (
                <Step key={item.title} title={item.title} />
              ))}
            </Steps>
            <div className="steps-content" style={{ height: 600 }}>
              {steps[current].content}
            </div>
          </Col>
        </Row>
        <Footer />
      </React.Fragment>
    </div>
  );
}

import React, { useEffect } from 'react'
import HeaderNav from '../../components/headerNav/headerNav'
import AddDocument from '../../components/addDocument/addDocument'
import Footer from '../../components/footer/footer'
import './home.scss'

export default function home() 
{
    
    return (
        <div className="container-home">
            <HeaderNav/>
            <AddDocument/>
            <Footer/>
        </div>
    );
}


import React, { useEffect } from 'react'
import {Row, Col} from 'antd'
import SideNav from '../../components/sideNav/sideNav'
import HeaderNav from '../../components/headerNav/headerNav'
import TableDocument from '../../components/tableDocument/tableDocument'
import Footer from '../../components/footer/footer'
export default function Deleted() 
{
    
    return (
        <div>
            <HeaderNav/>
            <Row>
                <Col span={5}><SideNav/></Col>
                <Col span={19}>
                    <TableDocument title='DELETED'/>
                </Col>
            </Row>
            
            <Footer/>
        </div>
    );
}


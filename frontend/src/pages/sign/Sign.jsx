import React, { useState, useEffect } from 'react'
import { Button } from 'antd'

import HeaderNav from '../../components/headerNav/headerNav'
import SignDocument from '../../components/signDocument/signDocument'

import './Sign.css'

export default function MakeSignature() {
    return (
        <div>
            <HeaderNav />
            <div className="document">
                <SignDocument />
            </div>
        </div>
    )
}
import React, { useState, useEffect } from 'react'
import { Button } from 'antd'

import HeaderNav from '../../components/headerNav/headerNav'
import Signature from '../../components/signature/signature'
import './MakeSignature.css'

export default function MakeSignature() {
    return (
        <div>
            <HeaderNav />
            <div className="stage" >
                <Signature />
            </div>
        </div>
    )
}
export function setUserIdToStorage(userId) {
  sessionStorage.setItem('userId', userId);
}

export function getUserIdFromStorage() {
  return JSON.parse(localStorage.getItem('user'))._id;
}

export function setUserFullNameToStorage(name) {
  sessionStorage.setItem('name', name);
}

export function getUserFullNameToStorage() {
  return sessionStorage.getItem('name');
}

export function setJwtToStorage(jwt) {
  sessionStorage.setItem('jwt', jwt);
}

export function getJwtFromStorage() {
  return sessionStorage.getItem('jwt');
}

export function clearStorage() {
  sessionStorage.clear();
}

export function isAuthenticated() {
  const jwt = getJwtFromStorage();
  return isEmptyString(jwt);
}

export function isEmptyString(prop) {
  return prop === null || prop === '';
}

export const getTokenFromStorage = () => {
  return localStorage.getItem('token');
};

export const getUserFromStorage = () => {
  return JSON.parse(localStorage.getItem('user'));
};

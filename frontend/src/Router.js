import React, { useEffect } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import Dashboard from './pages/dashboard/dashboard';
import Login from './components/login/login';
import Register from './components/register/register';
import PrepareEnvelope from './pages/prepareEnvelope/prepareEnvelope';
import Inbox from './pages/inbox/inbox';
import Sent from './pages/sent/sent';
import Draft from './pages/draft/draft';
import Deleted from './pages/deleted/deleted';
import ActionRequired from './pages/actionRequired/actionRequired';
import WaitingOthers from './pages/waitingOthers/waitingOthers';
import Complete from './pages/complete/complete';
import Signature from './components/signature/signature';
import MakeSignature from './pages/sign/MakeSignature';
import Sign from './pages/sign/Sign';

import * as actions from './store/actions';

const Router = (props) => {
  const { isAuthenticated, onTryAutoLogin } = props;

  useEffect(() => {
    onTryAutoLogin();
  }, [onTryAutoLogin]);

  let routes = (
    <Switch>
      <Route exact path="/" render={() => <Redirect to="/login" />} />
      <Route exact path="/login" component={Login} />
      <Route exact path="/register" component={Register} />
      {/* <Redirect to="/login" /> */}
    </Switch>
  );

  if (isAuthenticated) {
    routes = (
      <Switch>
        <Route exact path="/" component={Dashboard} />
        {/* <Route exact path="/profile" component={Login} /> */}
        <Route
          exact
          path="/prepare/:envelopeId/add-document"
          component={PrepareEnvelope}
        />
        <Route
          exact
          path="/prepare/:envelopeId/add-recipients"
          component={PrepareEnvelope}
        />
        {/* <Route
          exact
          path="/prepare/:envelopeId:/add-fields"
          component={PrepareEnvelope}
        /> */}
        <Route
          exact
          path="/prepare/:envelopeId/finish-and-send"
          component={PrepareEnvelope}
        />
        <Route exact path="/signature" component={MakeSignature}/>
        <Route exact path="/sign" component={Sign}/>
        <Route exact path="/inbox" component={Inbox} />
        <Route exact path="/sent" component={Sent} />
        <Route exact path="/draft" component={Draft} />
        <Route exact path="/deleted" component={Deleted} />
        <Route exact path="/action-required" component={ActionRequired} />
        <Route exact path="/waiting-others" component={WaitingOthers} />
        <Route exact path="/completed" component={Complete} />
        <Redirect to="/" />
      </Switch>
    );
  }

  return routes;
};

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.auth.token !== null,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onTryAutoLogin: () => dispatch(actions.authCheckState()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Router);

import React, { useState, useEffect } from 'react';
import { Modal, Form, Icon, Input, Button, Checkbox, Alert } from 'antd';

import "./reviewDocument.css";

export function ReviewDocument({
    visible,
    onOk,
    onCancel,
    login,
    customer,
    showModalRegister,
    showModalForgotPassword
}) {

    return (
        <div className="background">
            <div>
                <div className="content-2">
                    <div className="field-2">Email subject</div>
                    <div className="required"> *</div>
                </div>
                <div className="content-2">
                    <input className="input-1"></input>
                </div>
                <div className="content-2">
                    <div className="field-2">Description</div>
                </div>
                <div className="content-2">
                    <input className="input-2"></input>
                </div>
            </div>
            <div className="text-right footerrrr">
                <Button className="back-buttonnn">BACK</Button>
                <Button className="next-buttonnn">NEXT</Button>
            </div>
        </div >
    );
}

export default ReviewDocument

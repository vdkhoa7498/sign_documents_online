import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Orientation } from 'react-screen-orientation'
import {UserOutlined, DownloadOutlined} from '@ant-design/icons'
import { Menu, Row, Col, Button } from 'antd'

import './headerNav.scss';

export default function HeaderNav({
    selectDefault
}) {
    const { SubMenu } = Menu;

    useEffect(() => {

    })

    return (
        <Row className="container-header">
            <Col span={8}>
                <div className="title">
                    FONTT SIGN
                </div >
            </Col>
            <Col span={15}>
            <div className="center">
            <ul className="ul">
              <li><Link className="link-nav" to="/">Home</Link></li>
              <li><Link className="link-nav" to="/dashboard">Dashboard</Link></li>
              <li>About Us</li>
              <li>Help</li>
              <li>Contacts</li>
            </ul>
          </div>
            </Col>
            <Col span={1}>
                
            <Button className="user-button" type="default" shape="square" icon={<UserOutlined />} size='large' />
        
            </Col>
        </Row>
    );
}


import * as React from "react";
import { Link } from "react-router-dom"

import "./footer.scss";
import {Divider } from 'antd'

export default function Footer({ }) {

  return (
    <div className="footer-customer">
      <Divider className="divider-footer-customer"/>
      <div className="container-footer-customer">
        <div className="top">
          <div className="left">
            <Link className="link-home" to="/">FONTT SIGN</Link>
          </div>

          <div className="center">
            <ul className="ul">
              <li>Home</li>
              <li>Dashboard</li>
              <li>About Us</li>
              <li>Help</li>
              <li>Contacts</li>
            </ul>
          </div>
        </div>

      </div>
    </div>
  );
}


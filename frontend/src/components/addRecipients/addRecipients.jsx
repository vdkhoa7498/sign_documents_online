import React, { useState, useEffect } from 'react';
import { Menu, Dropdown, Icon, Input, Button, Checkbox, Alert } from 'antd';

import './addRecipients.css';

function AddRecipients({
  recipientName,
  recipientEmail,
  role,
  setRecipientName,
  setRecipientEmail,
  setRole,
  didAddRecipient,
  envelope,
}) {
  const userMenu = (
    <Menu>
      <Menu.Item
        key="1"
        onClick={() => {
          setRole('Needs to Sign');
        }}
      >
        Needs to Sign
      </Menu.Item>
      <Menu.Item
        key="2"
        onClick={() => {
          setRole('In Person Signer');
        }}
      >
        In Person Signer
      </Menu.Item>
      <Menu.Item
        key="3"
        onClick={() => {
          setRole('Receives a Copy');
        }}
      >
        Receives a Copy
      </Menu.Item>
    </Menu>
  );
  return (
    <div>
      <div className="background-2">
        <div className="content">
          <div className="field">Name</div>
          <div className="required"> *</div>
        </div>
        <div className="content">
          {didAddRecipient ? (
            <input
              disabled
              name="recipient-name"
              value={envelope.recipients[0].name}
              onChange={(e) => setRecipientName(e.target.value)}
              className="input"
            ></input>
          ) : (
            <input
              name="recipient-name"
              value={recipientName}
              onChange={(e) => setRecipientName(e.target.value)}
              className="input"
            ></input>
          )}

          <Dropdown.Button
            style={{ marginLeft: '5%' }}
            className="dropdown-btn"
            overlay={userMenu}
          >
            {role}
          </Dropdown.Button>
        </div>
        <div className="content">
          <div className="field">Email</div>
          <div className="required"> *</div>
        </div>
        <div className="content">
          {didAddRecipient ? (
            <input
              disabled
              name="recipient-email"
              value={envelope.recipients[0].user.email}
              onChange={(e) => setRecipientEmail(e.target.value)}
              className="input"
            ></input>
          ) : (
            <input
              name="recipient-email"
              value={recipientEmail}
              onChange={(e) => setRecipientEmail(e.target.value)}
              className="input"
            ></input>
          )}
        </div>
      </div>
      <div className="button">
        <div className="content-background">
          <div className="button-content">
            <i className="fas fa-user-plus"></i>
          </div>
          <div className="button-content text2">ADD RECIPIENT</div>
        </div>
      </div>
    </div>
  );
}

export default AddRecipients;

import React, { useRef, useState } from 'react';
import { Stage, Layer, Rect, Text, Line } from 'react-konva';
import { Button } from 'antd'
import './signature.css'

function downloadURI(uri, name) {
  // var link = document.createElement('a');
  // link.download = name;
  // link.href = uri;
  // document.body.appendChild(link);
  // link.click();
  // document.body.removeChild(link);
}

const Signnature = () => {
  const [tool, setTool] = useState('pen');
  const [lines, setLines] = useState([]);
  const isDrawing = useRef(false);

  const stageRef = useRef(null);

  const handleExport = () => {
    // const uri = stageRef.current.toDataURL();
    // console.log(uri);
    stageRef.current.toImage({
      callback(img) {
        console.log(img);
      },
    });
    // we also can save uri as file
    // but in the demo on Konva website it will not work
    // because of iframe restrictions
    // but feel free to use it in your apps:
    // downloadURI(uri, 'stage.png');
  };

  const handleMouseDown = (e) => {
    isDrawing.current = true;
    const pos = e.target.getStage().getPointerPosition();
    setLines([...lines, { tool, points: [pos.x, pos.y] }]);
  };

  const handleMouseMove = (e) => {
    // no drawing - skipping
    if (!isDrawing.current) {
      return;
    }
    const stage = e.target.getStage();
    const point = stage.getPointerPosition();
    let lastLine = lines[lines.length - 1];
    // add point
    lastLine.points = lastLine.points.concat([point.x, point.y]);

    // replace last
    lines.splice(lines.length - 1, 1, lastLine);
    setLines(lines.concat());
  };

  const handleMouseUp = () => {
    isDrawing.current = false;
  };

  return (
    <div>
      <button onClick={handleExport}>Click here to log stage data URL</button>
      <Stage
        width={250}
        height={250}
        border={'1px solid'}
        onMouseDown={handleMouseDown}
        onMousemove={handleMouseMove}
        onMouseup={handleMouseUp}
        ref={stageRef}
      >
        <Layer>
          <Text text="Just start drawing" x={5} y={30} />
          {lines.map((line, i) => (
            <Line
              key={i}
              points={line.points}
              stroke="#df4b26"
              strokeWidth={5}
              tension={0.5}
              lineCap="round"
              globalCompositeOperation={
                line.tool === 'eraser' ? 'destination-out' : 'source-over'
              }
            />
          ))}
        </Layer>
      </Stage>
      <br />
      <select
        value={tool}
        onChange={(e) => {
          setTool(e.target.value);
        }}
      >
        <option value="pen">Pen</option>
        <option value="eraser">Eraser</option>
      </select>
      <br /><br /><br />
      <Button className="next-button">
        NEXT
      </Button>
    </div>
  );
};

export default Signnature;

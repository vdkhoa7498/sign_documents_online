import React, { useState, useEffect } from 'react';
import { Button, message as antMessage } from 'antd';
import AddRecipients from '../addRecipients/addRecipients';

import './Recipient.css';
import { addRecipientsToEnvelope } from '../../services/envelope';
import { useHistory } from 'react-router-dom';

function Recipient({ envelope, onPrev, onNext }) {
  const history = useHistory();
  const [didAddRecipient, setDidAddRecipient] = useState(false);
  const [recipientName, setRecipientName] = useState('');
  const [recipientEmail, setRecipientEmail] = useState('');
  const [role, setRole] = useState('Needs to Sign');

  useEffect(() => {
    if (envelope.recipients.length !== 0) {
      setDidAddRecipient(true);
    }
  }, [envelope]);

  const handleSubmit = (event) => {
    event.preventDefault();
    if (didAddRecipient) {
      history.push(`/prepare/${envelope.envelopeId}/finish-and-send`);
      onNext();
    } else {
      addRecipientsToEnvelope(
        recipientName,
        recipientEmail,
        role,
        envelope.envelopeId
      )
        .then(({ success, message, updatedEnvelope }) => {
          if (success) {
            history.push(`/prepare/${envelope.envelopeId}/finish-and-send`);
            onNext();
            setDidAddRecipient(true);
          } else {
            console.log(message);
            antMessage.error(message);
          }
        })
        .catch((errorMessage) => {
          console.log(errorMessage);
          antMessage.error(errorMessage);
        });
    }
  };

  return (
    <form onSubmit={handleSubmit}>
      <div className="backgrounddd">
        <div>
          <div className="inline">
            <div className="round">
              <i className="far fa-paper-plane"></i>
            </div>
          </div>
          <div className="inline-2">
            <div className="header">SENDER</div>
            <div className="detail">{envelope.sender.user.name}</div>
          </div>
        </div>
        <div className="v1"></div>
        <div>
          <div className="inline-3">
            <div className="round">
              <i className="far fa-user"></i>
            </div>
          </div>
          <div className="inline-2">
            <div className="header">RECIPIENTS</div>
          </div>
        </div>
        <div className="addrece">
          <AddRecipients
            didAddRecipient={didAddRecipient}
            recipientName={recipientName}
            recipientEmail={recipientEmail}
            setRecipientName={setRecipientName}
            setRecipientEmail={setRecipientEmail}
            role={role}
            setRole={setRole}
            envelope={envelope}
          />
        </div>
        <div className="v2"></div>
        <div>
          <div className="inline-4">
            <div className="round">
              <i className="far fa-paper-plane"></i>
            </div>
          </div>
          <div className="inline-2">
            <div className="detail">
              Once an envelope has been routed to all recipients, and documents
              signed, each recipient will get a completed copy.
            </div>
          </div>
        </div>
        <div style={{ marginTop: '1%' }}>
          <Button
            className="back-button"
            onClick={() => {
              history.push(`/prepare/${envelope.envelopeId}/add-document`);
              onPrev();
            }}
          >
            BACK
          </Button>
          <Button className="next-button" htmlType="submit">
            NEXT
          </Button>
        </div>
      </div>
    </form>
  );
}

export default Recipient;

import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import {
  Modal,
  Form,
  Icon,
  Input,
  Button,
  Checkbox,
  Alert,
  Upload,
  message,
} from 'antd';
import { getTokenFromStorage, getUserIdFromStorage } from '../../utils/utils';
import { InboxOutlined, UploadOutlined } from '@ant-design/icons';

import './addDocument.scss';

function beforeUpload(file) {
  const isPdfOrWord =
    file.type === 'application/pdf' || file.type === 'application/msword';
  if (!isPdfOrWord) {
    message.error('You can only upload PDF/WORD file!');
  }
  const isLt2M = file.size / 1024 / 1024 < 2;
  if (!isLt2M) {
    message.error('File must smaller than 2MB!');
  }
  return isPdfOrWord && isLt2M;
}

export function AddDocument({ onNext, envelope }) {
  const history = useHistory();

  const [didUpload, setDidUpload] = useState(false);
  const [fileList, setFileList] = useState(null);

  let action = `http://localhost:4000/envelope/`;

  if (envelope) {
    action = `http://localhost:4000/envelope/${envelope.envelopeId}/add-document`;
  }

  const uploadProps = {
    name: 'document',
    action: action,
    method: 'PUT',
    headers: {
      authorization: `Bearer ${getTokenFromStorage()}`,
    },
    multiple: false,
    onChange(info) {
      if (info.file.status !== 'uploading') {
        console.log(info.file, info.fileList);
      }
      if (info.file.status === 'done') {
        message.success(`${info.file.name} file uploaded successfully`);
        setDidUpload(true);
      } else if (info.file.status === 'error') {
        message.error(`${info.file.name} file upload failed.`);
      }
    },
  };
  useEffect(() => {
    if (envelope) {
      if (envelope.file) {
        setDidUpload(true);
        setFileList([
          {
            uid: '1',
            name: envelope.file.name,
            status: 'done',
          },
        ]);
      }
    }
  }, [envelope]);

  return (
    <div className="background-addDoc">
      <div className="upload">
        <i className="fas fa-file-upload"></i>
      </div>
      {/* <div className="dropfile">Drops your file here</div> */}
      <div className="browse">
        {didUpload && fileList ? (
          <>
            {console.log(fileList)}
            <Upload
              {...uploadProps}
              defaultFileList={fileList}
              beforeUpload={beforeUpload}
              disabled
            >
              <Button icon={<UploadOutlined />} disabled>
                Click to Upload
              </Button>
            </Upload>
          </>
        ) : (
          <Upload {...uploadProps} beforeUpload={beforeUpload}>
            <Button icon={<UploadOutlined />}>Click to Upload</Button>
          </Upload>
        )}
      </div>
      <div className="text-right footer">
        <div className="check">
          <Checkbox>I'm the only signer</Checkbox>
        </div>
        <div className="next">
          <Button
            onClick={() => {
              if (didUpload) {
                // onNext();
                history.push(`/prepare/${envelope.envelopeId}/add-recipients`);
                onNext();
              } else {
                message.error('You need to upload 1 document first!');
              }
            }}
            className="next-buttonn"
          >
            NEXT
          </Button>
        </div>
      </div>
    </div>
  );
}

export default AddDocument;

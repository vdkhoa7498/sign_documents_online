import React, { useState, useCallback, useEffect } from 'react';
import { Redirect, useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import { Form, Input, Button, Alert, message } from 'antd';
import { LockOutlined, LoginOutlined, UserOutlined } from '@ant-design/icons';

import * as actions from '../../store/actions';
import './login.scss';

export function Login(props) {
  const {
    onLoginWithEmailAndPassword,
    onClearError,
    onResetAuthRedirectPath,
    authError,
    authRedirectPath,
    loading,
  } = props;

  useEffect(() => {
    if (authRedirectPath) {
      history.push('/');
      message.success('Đăng nhập thành công');
      onResetAuthRedirectPath();
    }
  }, [authRedirectPath]);

  const history = useHistory();

  const [form] = Form.useForm();
  const handleSubmit = (data) => {
    const { email, password } = data;
    onLoginWithEmailAndPassword(email, password);
  };

  const Register = () => {
    history.push({
      pathname: '/register',
    });
  };

  const onCloseAlert = useCallback((e) => {
    onClearError();
  }, []);

  return (
    <div className="linear-gradient">
      <div className="step-background">
        <br />
        <div className="title">FONTT SIGN</div>
        <div className="element">
          <br />
          {authError && (
            <Alert
              message="Lỗi"
              className="alert-content"
              description={authError}
              type="error"
              closable
              showIcon
              onClose={onCloseAlert}
              style={{ marginBottom: '16px' }}
            />
          )}
          <Form onFinish={handleSubmit} className="login-form" form={form}>
            <Form.Item
              name="email"
              rules={[
                {
                  required: true,
                  message: 'Vui lòng nhập email!',
                  whitespace: false,
                },
              ]}
            >
              <Input
                prefix={
                  <UserOutlined
                    style={{
                      color: 'rgba(0,0,0,.25)',
                    }}
                  />
                }
                placeholder="Email"
                type="email"
              />
            </Form.Item>
            <Form.Item
              name="password"
              rules={[
                { required: true, message: '(*) Vui lòng nhập mật khẩu !' },
              ]}
            >
              <Input
                prefix={<LockOutlined style={{ color: 'rgba(0,0,0,.25)' }} />}
                type="password"
                placeholder="Mật khẩu"
              />
            </Form.Item>
            <Form.Item className="portal-button">
              <Button
                type="primary"
                htmlType="submit"
                className="login-form-button"
                icon={<LoginOutlined />}
                loading={loading}
              >
                Đăng nhập
              </Button>
            </Form.Item>
            <div className="align-right">
              <div className="gglogin">
                <i className="fab fa-google-plus-square"></i>
              </div>
            </div>

            <br />
            <div className="bottom register">
              <p className="text inline">Bạn chưa có tài khoản?</p>
              <Button className="register" type="link" onClick={Register}>
                Đăng ký ngay
              </Button>
            </div>
          </Form>
        </div>
      </div>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.auth.token !== null,
    loading: state.auth.loading,
    authError: state.auth.error,
    authRedirectPath: state.auth.authRedirectPath,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onLoginWithEmailAndPassword: (email, password) =>
      dispatch(actions.authWithEmailAndPassword(email, password)),
    onClearError: () => dispatch(actions.authClearError()),
    onResetAuthRedirectPath: () => dispatch(actions.resetAuthRedirectPath()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);

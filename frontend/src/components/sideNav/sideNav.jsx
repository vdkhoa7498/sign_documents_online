import React, { useState, useEffect, useCallback } from 'react';
import { Button, Menu, message } from 'antd';
import { useHistory } from 'react-router';
import { connect } from 'react-redux';

import { createEmptyEnvelope } from '../../services/envelope';

import './sideNav.scss';

export function SideNav({ selectDefault, userId }) {
  const history = useHistory();

  function handleMenuClick(e) {
    switch (e.key) {
      case '1':
        history.push({
          pathname: '/inbox',
        });
        break;
      case '2':
        history.push({
          pathname: '/sent',
        });
        break;
      case '3':
        history.push({
          pathname: '/draft',
        });
        break;
      case '4':
        history.push({
          pathname: '/deleted',
        });
        break;
      case '5':
        history.push({
          pathname: '/action-required',
        });
        break;
      case '6':
        history.push({
          pathname: '/waiting-others',
        });
        break;
      case '7':
        history.push({
          pathname: '/completed',
        });
        break;
      default:
        console.log('default');
    }
  }

  useEffect(() => {});

  const onClickStartNowButtonHandler = useCallback(async () => {
    try {
      console.log(userId);
      const { success, envelope, message } = await createEmptyEnvelope(userId);
      if (success) {
        history.push(`/prepare/${envelope.envelopeId}/add-document`);
      } else {
        console.log(message);
      }
    } catch (errorMessage) {
      console.log(errorMessage);
    }
  }, []);

  return (
    <>
      <div className="side-nav">
        <br />
        <div className="center">
          {/* <div className="button">START NOW</div> */}
          <Button
            type="primary"
            style={{ height: '45px' }}
            onClick={onClickStartNowButtonHandler}
          >
            START NOW
          </Button>
        </div>
        <Menu
          onClick={handleMenuClick}
          className="menu-seller"
          selectedKeys={[`${selectDefault}`]}
          defaultOpenKeys={['sub1']}
          mode="inline"
        >
          <div className="side-title">ENVELOPES</div>
          <Menu.Item key="1">
            <i className="fas fa-inbox icon-side"></i>
            <span>Inbox</span>
          </Menu.Item>
          <Menu.Item key="2">
            <i className="far fa-paper-plane icon-side"></i>
            <span>Sent</span>
          </Menu.Item>
          <Menu.Item key="3">
            <i className="far fa-edit icon-side"></i>
            <span>Draft</span>
          </Menu.Item>
          <Menu.Item key="4">
            <i className="far fa-trash-alt icon-side"></i>
            <span>Deleted</span>
          </Menu.Item>
          <div className="side-title">QUICK VIEWS</div>
          <Menu.Item key="5">
            <i className="fas fa-exclamation icon-side"></i>
            <span>Action Required</span>
          </Menu.Item>
          <Menu.Item key="6">
            <i className="fas fa-spinner icon-side"></i>
            <span>Waiting for Others</span>
          </Menu.Item>
          <Menu.Item key="7">
            <i className="far fa-check-circle icon-side"></i>
            <span>Completed</span>
          </Menu.Item>
        </Menu>
      </div>
    </>
  );
}

const mapStateToProps = (state) => {
  return {
    userId: state.auth.user._id,
  };
};

export default connect(mapStateToProps)(SideNav);

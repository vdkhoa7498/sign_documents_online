import React, { useState, useCallback } from 'react';
import { LockOutlined, UserAddOutlined, UserOutlined } from '@ant-design/icons';
import { Form, Input, Button, Alert, message } from 'antd';

import { useHistory } from 'react-router';
import './register.scss';
import { handleRegister } from '../../services/register';

export function Register() {
  const history = useHistory();
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);
  const [registerError, setRegisterError] = useState(null);

  const Login = () => {
    history.push({
      pathname: '/login',
    });
  };

  const onCloseAlert = useCallback((e) => {
    setRegisterError(null);
  }, []);

  const handleSubmit = (data) => {
    setLoading(true);
    handleRegister(data.email, data.name, data.password)
      .then((value) => {
        setLoading(false);
        history.push({
          pathname: '/login',
        });
        message.success(
          'Đăng kí thành công. Bạn có thể đăng nhập ngay bây giờ !'
        );
      })
      .catch((errorMessage) => {
        setRegisterError(errorMessage);
        setLoading(false);
        form.resetFields();
      });
  };

  return (
    <div className="linear-gradient">
      <div className="step-background">
        <br />
        <div className="title">FONTT SIGN</div>
        <div className="element">
          <br />
          {registerError ? (
            <Alert
              message="Lỗi"
              description={registerError}
              type="error"
              closable
              showIcon
              onClose={onCloseAlert}
              style={{ marginBottom: '16px' }}
            />
          ) : null}

          <Form
            onFinish={handleSubmit}
            className="login-form element"
            form={form}
          >
            <Form.Item
              name="email"
              rules={[
                {
                  required: true,
                  message: 'Vui lòng nhập email!',
                  whitespace: false,
                },
              ]}
            >
              <Input
                prefix={
                  <UserOutlined
                    style={{
                      color: 'rgba(0,0,0,.25)',
                    }}
                  />
                }
                placeholder="Email"
                type="email"
              />
            </Form.Item>
            <Form.Item
              name="name"
              rules={[
                {
                  required: true,
                  message: 'Vui lòng nhập tên đầy đủ của bạn!',
                  whitespace: true,
                },
              ]}
            >
              <Input
                prefix={
                  <UserOutlined
                    style={{
                      color: 'rgba(0,0,0,.25)',
                    }}
                  />
                }
                placeholder="Tên của bạn"
                maxLength="45"
              />
            </Form.Item>

            <Form.Item
              name="password"
              rules={[
                {
                  required: true,
                  message: 'Vui lòng nhập mật khẩu!',
                },
                {
                  pattern: /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*/,
                  message:
                    'Mật khẩu phải bao gồm 1 chữ thường, 1 số và 1 chữ hoa!',
                },
                {
                  min: 6,
                  max: 32,
                  message: 'Mật khẩu phải từ 6 đến 32 ký tự!',
                },
              ]}
              hasFeedback
            >
              <Input.Password
                prefix={
                  <LockOutlined
                    style={{
                      color: 'rgba(0,0,0,.25)',
                    }}
                  />
                }
                placeholder="Mật khẩu"
                maxLength="60"
              />
            </Form.Item>
            <Form.Item
              name="confirm"
              dependencies={['password']}
              hasFeedback
              rules={[
                {
                  required: true,
                  message: 'Vui lòng nhập mật khẩu xácnhậnn!',
                },
                ({ getFieldValue }) => ({
                  validator(rule, value) {
                    if (!value || getFieldValue('password') === value) {
                      return Promise.resolve();
                    }

                    return Promise.reject('Mật khẩu xác nhận chưa khớp');
                  },
                }),
              ]}
            >
              <Input.Password
                prefix={
                  <LockOutlined
                    style={{
                      color: 'rgba(0,0,0,.25)',
                    }}
                  />
                }
                placeholder="Mật khẩu xác nhận"
              />
            </Form.Item>
            <Form.Item>
              <Button
                type="primary"
                htmlType="submit"
                className="login-form-button"
                icon={<UserAddOutlined />}
                loading={loading}
              >
                SIGN UP
              </Button>
            </Form.Item>
            <br />
            <div className="bottom register">
              <p className="text inline">Bạn đã có tài khoản?</p>
              <Button className="register" type="link" onClick={Login}>
                Đăng nhập ngay
              </Button>
            </div>
          </Form>

          {/* <Form onSubmit={handleFormSubmit} className="element" form={form}>
                        <Form.Item className="input-content" label="E-MAIL">
                            <Input className="input-login"
                                placeholder="Enter your e-mail"
                                name="email"
                            />
                        </Form.Item>

                        <Form.Item className="input-content" label="PASSWORD">
                            <Input className="input-login"
                                type="password"
                                placeholder="Enter your password"
                                name="password"
                            />
                        </Form.Item>

                        <Form.Item className="input-content" label="CONFIRM PASSWORD">
                            <Input className="input-login"
                                type="password"
                                placeholder="Re-enter your password"
                                name="password"
                            />
                        </Form.Item>

                        <br />
                        <div className="footer login">
                            <Button type="primary" htmlType="submit" className="login-form-button login-button">
                                SIGN UP
                        </Button>
                        </div>
                        <br />
                        <div className="bottom register">
                            <p className="text inline">Already have an account?</p>
                            <Button className="register" type="link" onClick={Login}>Log in</Button>
                        </div>
                    </Form>
                 */}
        </div>
      </div>
    </div>
  );
}

export default Register;

import React, {useEffect, useState} from 'react'
import {Dropdown, Input, Menu, Table} from 'antd'
import {api} from '../../services/api';
import './tableDocument.scss'

const {Column} = Table;

export default function TableDocument(props) {


    // const data = [
    //     {
    //         key: '1',
    //         subject: 'ABC.docx',
    //         status: 'Completed',
    //         lastChange: '11/11/2020',
    //         action: 'Edit'
    //     }, {
    //         key: '2',
    //         subject: 'XYZ.docx',
    //         status: 'Completed',
    //         lastChange: '11/11/2020',
    //         action: 'Delete'
    //     }, {
    //         key: '3',
    //         subject: 'ABC123.docx',
    //         status: 'Completed',
    //         lastChange: '11/11/2020',
    //         action: 'Edit'
    //     },
    // ];

    useEffect(() => {
        const getEnvelopes = async () => {
            const userInfo = JSON.parse(localStorage.getItem('user'));
            const userId = userInfo._id;
            const res = await api.getWithToken(`/envelope/${props.status}?userId=${userId}`)

            const resData = res.data[props.status].map((envelope, index)=>{
                return {
                    key: index,
                    status: envelope.sender.envelopeStatus,
                    subject: envelope.file.name,
                    lastChange: envelope.updatedAt,
                }
            })
            setListEnvelope(resData);
            console.log(res.data[props.status]);
        }

        getEnvelopes();
    }, [])

    const [action, setAction] = useState("")
    const [listEnvelope, setListEnvelope] = useState([]);

    const actionList = ["Edit", "Void", "Delete"]

    function handleMenuClick(e) {
        setAction(e.key)
        console.log('click', e);
    }

    const menu = (
        <Menu onClick={handleMenuClick}>
            {actionList.map((actionItem) => <Menu.Item key={actionItem}>{actionItem}</Menu.Item>)}
        </Menu>
    );


    return (

        <>
            <div className="header">
                <div className="title">{props.title}</div>
                <Input.Group className="search-box" compact>
                    <Input.Search allowClear style={{width: '40%'}} defaultValue=""/>
                </Input.Group>
            </div>

            <Table dataSource={listEnvelope}>

                <Column title="Subject" dataIndex="subject" key="subject"/>
                <Column title="Status" dataIndex="status" key="status"/>
                <Column title="LastChange" dataIndex="lastChange" key="lastChange"/>

                <Column
                    title="Action"
                    key="action"
                    render={() => (

                        <Dropdown.Button overlay={menu}>
                            {action}
                        </Dropdown.Button>
                    )}
                />
            </Table>
        </>
    );
}
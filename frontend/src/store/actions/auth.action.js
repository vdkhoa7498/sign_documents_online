import { handleLogin } from '../../services/login';
import * as actionTypes from './actionTypes';
import { getUserFromStorage } from '../../utils/utils';

const processResponseWhenLoginSuccess = (dispatch, data) => {
  if (data) {
    const expirationDate = new Date(new Date().getTime() + 3600 * 1000);
    localStorage.setItem('token', data.accessToken);
    localStorage.setItem('expirationDate', expirationDate);
    localStorage.setItem('user', JSON.stringify(data.user));
    dispatch(authSuccess(data.accessToken, data.user));
    dispatch(checkAuthTimeout(3600));
  }
};

const processErrWhenLoginFail = (dispatch, errorMessage) => {
  dispatch(authFail(errorMessage));
};

export const setAuthRedirectPath = () => {
  return {
    type: actionTypes.SET_AUTH_REDIRECT_PATH,
  };
};

export const authClearError = () => {
  return {
    type: actionTypes.AUTH_CLEAR_ERRROR,
  };
};

export const authStart = () => {
  return {
    type: actionTypes.AUTH_START,
  };
};

export const authSuccess = (token, user) => {
  return {
    type: actionTypes.AUTH_SUCCESS,
    token,
    user,
  };
};

export const authFail = (error) => {
  return {
    type: actionTypes.AUTH_FAIL,
    error,
  };
};

export const logout = () => {
  return {
    type: actionTypes.AUTH_LOGOUT,
  };
};

export const checkAuthTimeout = (experationTime) => {
  return (dispatch) => {
    setTimeout(() => {
      dispatch(logout());
    }, experationTime * 1000);
  };
};

export const authWithEmailAndPassword = (email, password) => {
  return (dispatch) => {
    dispatch(authStart());
    handleLogin(email, password)
      .then((data) => {
        dispatch(setAuthRedirectPath());
        processResponseWhenLoginSuccess(dispatch, data);
      })
      .catch((errorMessage) => {
        processErrWhenLoginFail(dispatch, errorMessage);
        // setLoading(false);
        // setLoginErrorMessage(errorMessage);
      });
  };
};

export const authCheckState = () => {
  return (dispatch) => {
    const token = localStorage.getItem('token');

    if (!token) {
      dispatch(logout());
    } else {
      const expirationDate = new Date(localStorage.getItem('expirationDate'));
      if (expirationDate <= new Date()) {
        dispatch(logout());
      } else {
        dispatch(authSuccess(token, getUserFromStorage()));
        dispatch(
          checkAuthTimeout(
            (expirationDate.getTime() - new Date().getTime()) / 1000
          )
        );
      }
    }
  };
};

export const resetAuthRedirectPath = () => {
  return {
    type: actionTypes.RESET_AUTH_REDIRECT_PATH,
  };
};

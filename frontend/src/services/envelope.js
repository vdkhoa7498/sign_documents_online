import { api } from './api';

export function createEmptyEnvelope(senderId) {
  return new Promise((resolve, reject) => {
    api
      .postWithToken(`/envelope/`, {
        senderId,
      })
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => {
        if (error.response) {
          reject(error.response.data.message);
        } else {
          reject(error.message);
        }
      });
  });
}

export function addRecipientsToEnvelope(
  recipientName,
  recipientEmail,
  recipientType,
  envelopeId
) {
  return new Promise((resolve, reject) => {
    api
      .putWithToken(`/envelope/${envelopeId}/add-recipients`, {
        recipientName,
        recipientEmail,
        recipientType,
      })
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => {
        if (error.response) {
          reject(error.response.data.message);
        } else {
          reject(error.message);
        }
      });
  });
}

export function getEnvelopeById(envelopeId) {
  return new Promise((resolve, reject) => {
    api
      .getWithToken(`/envelope/${envelopeId}`)
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => {
        if (error.response) {
          reject(error.response.data.message);
        } else {
          reject(error.message);
        }
      });
  });
}

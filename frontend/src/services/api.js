import axios from 'axios';
import { getJwtFromStorage, getTokenFromStorage } from '../utils/utils';

export const host = 'http://localhost:4000';

const instance = axios.create({
  baseURL: host,
});

export const api = {
  get: (url) => {
    return instance.get(`${url}`);
  },

  getWithToken: (url) => {
    const token = getTokenFromStorage();
    const config = {
      headers: { Authorization: `Bearer ${token}` },
    };
    return instance.get(`${url}`, config);
  },

  post: (url, req) => {
    return instance.post(`${url}`, req);
  },

  postWithToken: (url, req) => {
    const token = getTokenFromStorage();
    const config = {
      headers: { Authorization: `Bearer ${token}` },
    };
    return instance.post(`${url}`, req, config);
  },

  put: (url, req) => {
    return instance.put(`${url}`, req);
  },

  putWithToken: (url, req) => {
    const token = getTokenFromStorage();
    const config = {
      headers: { Authorization: `Bearer ${token}` },
    };
    return instance.put(`${url}`, req, config);
  },

  delete: (url, req) => {
    return instance.delete(`${url}`, req);
  },

  authGet: (url) => {
    let jwt = getJwtFromStorage();
    return instance.get(`${url}`, { headers: { Authorization: jwt } });
  },
  authPost: (url, req) => {
    let jwt = getJwtFromStorage();
    return instance.post(`${url}`, req, { headers: { Authorization: jwt } });
  },
  authPut: (url, req) => {
    let jwt = getJwtFromStorage();
    return instance.put(`${url}`, req, { headers: { Authorization: jwt } });
  },
};

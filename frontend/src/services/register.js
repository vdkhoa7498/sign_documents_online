import { api } from './api';

export function handleRegister(email, name, password) {
  return new Promise((resolve, reject) => {
    api
      .post('auth/register', {
        email,
        name,
        password,
      })
      .then((response) => {
        resolve();
      })
      .catch((error) => {
        reject(error.response.data.message);
      });
  });
}

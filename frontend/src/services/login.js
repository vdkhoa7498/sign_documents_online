import {
  setJwtToStorage,
  setUserIdToStorage,
  setUserFullNameToStorage,
} from '../utils/utils';
import { api } from './api';

export function handleLogin(email, password) {
  return new Promise((resolve, reject) => {
    api
      .post('/auth/login', {
        email,
        password,
      })
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => {
        reject(error.response.data.message);
      });
  });
}

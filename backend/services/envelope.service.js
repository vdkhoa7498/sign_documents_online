const httpStatus = require('http-status');
const fs = require('fs');
const { v4: uuidv4 } = require('uuid');

const { Envelope } = require('../models');
const ApiError = require('../utils/ApiError');
const userService = require('./user.service');
const { envelopeService } = require('.');

const getEnvelopeByEnvelopeId = async (envelopeId) => {
  const envelope = await Envelope.findOne({ envelopeId })
    .populate('sender.user')
    .populate('recipients.user');
  if (!envelope) {
    throw new ApiError(
      httpStatus.NOT_FOUND,
      'Không thể tìm thấy envelope tương ứng!'
    );
  }
  return envelope;
};

const createEmptyEnvelope = async (senderId) => {
  const envelope = new Envelope({
    envelopeId: uuidv4(),
    sender: {
      user: senderId,
      envelopeStatus: 'Draft',
    },
  });
  await envelope.save();
  return envelope;
};

const addDocumentToEnvelope = async (envelopeFile, envelopeId) => {
  let envelope = await getEnvelopeByEnvelopeId(envelopeId);
  envelope.file.url = envelopeFile.path;
  envelope.file.name = envelopeFile.originalname;
  await envelope.save();
  return envelope;
};

const addRecipientsToEnvelope = async (envelope, recipientData) => {
  const { recipientName, recipientEmail, recipientType } = recipientData;
  const recipient = await userService.getUserByEmail(recipientEmail);
  console.log(recipient);
  const recipientArray = [];
  const addedRecipient = {
    user: recipient._id,
    name: recipientName,
    type: recipientType,
    didSign: false,
    envelopeStatus: 'Needs to sign',
  };
  recipientArray.push(addedRecipient);
  envelope.recipients = recipientArray;
  return envelope.save();
  // const recipientArray = [];
  // for (let i = 1; i <= recipientsData['count-recipients']; i++) {
  //   const recipient = await userService.getUserByEmail(
  //     recipientsData[`email${i}`]
  //   );
  //   const addedRecipient = {
  //     _id: recipient._id,
  //     recipientType: 'Needs to sign',
  //     didSign: false,
  //     envelopeStatusForRecipient: 'Needs to sign',
  //   };
  //   recipientArray.push(addedRecipient);
  // }
  // envelope.recipients = recipientArray;
  // return envelope.save();
};

const findEnvelopeById = async (envelopeId) => {
  return Envelope.findById(envelopeId);
};

const deleteEnvelope = async (envelopeId) => {
  const envelope = await findEnvelopeById(envelopeId);
  envelope.envelopeStatusForSender = 'Voided';
  return envelope.save();
};

const downloadEnvelope = async (envelopeId) => {
  const envelope = await findEnvelopeById(envelopeId);
  const file = fs.createReadStream(envelope.file.url);
  let extension = null;
  if (envelope.file.url.split('.')[1] === 'doc') {
    extension = 'msword';
  } else if (envelope.file.url.split('.')[1] === 'pdf') {
    extension = 'pdf';
  }
  const filename = envelope.file.url.split('\\')[2];
  return { file, extension, filename };
};

module.exports = {
  getEnvelopeByEnvelopeId,
  createEmptyEnvelope,
  addDocumentToEnvelope,
  addRecipientsToEnvelope,
  findEnvelopeById,
  deleteEnvelope,
  downloadEnvelope,
};

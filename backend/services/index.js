const authService = require('./auth.service');
const userService = require('./user.service');
const tokenService = require('./token.service');
const envelopeService = require('./envelope.service');

module.exports = {
  authService,
  userService,
  tokenService,
  envelopeService,
};

const httpStatus = require("http-status");
const bcrypt = require("bcryptjs");
const mongoose = require("mongoose");

const { User } = require("../models");
const ApiError = require("../utils/ApiError");

const createUser = async (userBody) => {
  if (await User.isEmailTaken(userBody.email)) {
    throw new ApiError(httpStatus.BAD_REQUEST, "Email đã có người sử dụng");
  }
  const { name, email, password } = userBody;
  const hashPassword = await bcrypt.hash(password, 12);
  const user = new User({
    name,
    email,
    password: hashPassword,
  });
  await user.save();
  return user;
};

const getUserByEmail = (email) => {
  return User.findOne({ email: email });
};

const getUserById = (id) => {
  return User.findById(id);
};

const addSignImageUrl = (user, signImageUrl) => {
  user.signImageUrl = signImageUrl;
  return user.save();
};

const findInboxes = async (userId) => {
  // return User.findById(userId, { inboxes: 1 });

  const user = await User.aggregate(
    [
      {
        $match: { _id: new mongoose.Types.ObjectId(userId) },
      },
      {
        $lookup: {
          from: "envelopes",
          localField: "inboxes",
          foreignField: "envelopeId",
          as: "inboxEnvelopes",
        },
      },
    ],
    (err, data) => {
      if (err) {
        throw new ApiError(httpStatus.BAD_REQUEST, err);
      } else {
        console.log("return data: ", data);
        return data;
      }
    }
  );

  return user[0];

  // return Envelope.find({
  //   $or: [
  //     { $and: [{ sender: userId }, { envelopeStatusForSender: 'Completed' }] },
  //     {
  //       $and: [
  //         { 'recipients._id': userId },
  //         {
  //           'recipients.envelopeStatusForRecipient': {
  //             $in: ['Completed', 'Needs to sign', 'Voided'],
  //           },
  //         },
  //       ],
  //     },
  //   ],
  // });
};

const findSents = async (userId) => {
  return User.findById(userId, { sents: 1 });
  // return Envelope.find({
  //   $and: [{ sender: userId }, { envelopeStatusForSender: 'On-going' }],
  // });
};

const findDrafts = async (userId) => {
  return User.findById(userId, { drafts: 1 });
  // return Envelope.find({
  //   $and: [{ sender: userId }, { envelopeStatusForSender: 'Draft' }],
  // });
};

const findDeleteds = async (userId) => {
  return User.findById(userId, { deleteds: 1 });
  // return Envelope.find({
  //   $and: [{ sender: userId }, { envelopeStatusForSender: 'Voided' }],
  // });
};

module.exports = {
  createUser,
  getUserByEmail,
  getUserById,
  addSignImageUrl,
  findInboxes,
  findSents,
  findDrafts,
  findDeleteds,
};

const httpStatus = require('http-status');
const bcrypt = require('bcryptjs');

const { User } = require('../models');
const ApiError = require('../utils/ApiError');
const userService = require('./user.service');

const loginUserWithEmailAndPassword = async (email, password) => {
  const user = await userService.getUserByEmail(email);
  if (!user || !(await user.isPasswordMatch(password))) {
    throw new ApiError(httpStatus.UNAUTHORIZED, 'Sai email hoặc mật khẩu!');
  }
  return user;
};

module.exports = {
  loginUserWithEmailAndPassword,
};

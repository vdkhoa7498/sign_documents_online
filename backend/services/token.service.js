const httpStatus = require('http-status');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const { User } = require('../models');
const ApiError = require('../utils/ApiError');

const generateAuthToken = async (user) => {
  return jwt.sign(
    {
      email: user.email,
      userId: user._id.toString(),
    },
    'mysupersecret',
    {
      expiresIn: '1h',
    }
  );
};

module.exports = {
  generateAuthToken,
};

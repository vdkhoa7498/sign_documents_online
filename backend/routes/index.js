const userRoute = require('./user.route');
const authRoute = require('./auth.route');
const envelopeRoute = require('./envelope.route');

module.exports = {
  userRoute,
  authRoute,
  envelopeRoute,
};

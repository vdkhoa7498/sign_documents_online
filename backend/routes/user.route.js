const express = require('express');
const passport = require('passport');

const { uploadSignImage } = require('../config/multer.config');
const userController = require('../controllers/user.controller');

const router = express.Router();

require('../config/passportJWT.config')(passport);

// Thêm chữ ký cho user
router.put(
  '/:userId/add-sign-image-url',
  passport.authenticate('jwt', { session: false }),
  uploadSignImage.single('sign-image'),
  userController.addSignImageUrl
);

module.exports = router;

const express = require('express');
const passport = require('passport');

const { uploadDocument } = require('../config/multer.config');

const envelopeController = require('../controllers/envelope.controller');

const router = express.Router();

require('../config/passportJWT.config')(passport);

// Lấy danh sách các envelope inbox của 1 user
router.get(
  '/inbox',
  passport.authenticate('jwt', { session: false }),
  envelopeController.getInboxes
);

// Lấy danh sách các envelope sent của 1 user
router.get(
  '/sent',
  passport.authenticate('jwt', { session: false }),
  envelopeController.getSents
);

//Lấy danh sách các envelope draft của 1 user
router.get(
  '/draft',
  passport.authenticate('jwt', { session: false }),
  envelopeController.getDrafts
);

//Lấy danh sách các envelope deleted của 1 user
router.get(
  '/deleted',
  passport.authenticate('jwt', { session: false }),
  envelopeController.getDeleteds
);

// Lấy thông tin envelope
router.get(
  '/:envelopeId',
  passport.authenticate('jwt', { session: false }),
  envelopeController.getEnvelopeByEnvelopeId
);

// Tạo empty envelope
router.post(
  '/',
  passport.authenticate('jwt', { session: false }),
  envelopeController.createEmptyEnvelope
);

//Upload document (Bước 1 trong giai đoạn tạo envelope)
router.put(
  '/:envelopeId/add-document',
  passport.authenticate('jwt', { session: false }),
  uploadDocument.single('document'),
  envelopeController.addDocumentToEnvelope
);

//Thêm các bên liên quan vào document cần ký (Bước 2 trong giai đoạn tạo envelope)
router.put(
  '/:envelopeId/add-recipients',
  passport.authenticate('jwt', { session: false }),
  envelopeController.addRecipientsToEnvelope
);

//Xóa envelope
router.delete(
  '/:envelopeId',
  passport.authenticate('jwt', { session: false }),
  envelopeController.deleteEnvelope
);

//Tải về envelope
router.get(
  '/:envelopeId/download',
  passport.authenticate('jwt', { session: false }),
  envelopeController.downloadEnvelope
);

module.exports = router;

const express = require('express');
const passport = require("passport")
const authController = require('../controllers/auth.controller');

const router = express.Router();

// Đăng ký người dùng
router.post('/register', authController.register);

// Đăng nhập người dùng (Email Password)
router.post('/login', authController.login);

/**
 * Login with Google
 */
router.get(
  '/auth/google',
  passport.authenticate('GoogleAuth', {
    scope:
      ['email', 'profile'],
  }),
);
router.get(
  '/auth/google/callback',
  passport.authenticate('GoogleAuth', {
    session: false,
    failureRedirect: '/',
  }),
  authController.authExternal,
);

module.exports = router;

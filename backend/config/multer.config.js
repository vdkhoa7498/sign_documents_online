const multer = require('multer');

const storageDocument = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, './data/document/');
  },
  filename: (req, file, cb) => {
    const uniquePrefix = Date.now() + '-' + Math.round(Math.random() * 1e9);
    cb(
      null,
      uniquePrefix +
        '-' +
        file.fieldname +
        '.' +
        file.originalname.split('.')[1]
    );
  },
});

const storageSignImage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, './images/');
  },
  filename: (req, file, cb) => {
    const uniquePrefix = Date.now() + '-' + Math.round(Math.random() * 1e9);
    cb(
      null,
      uniquePrefix +
        '-' +
        file.fieldname +
        '.' +
        file.originalname.split('.')[1]
    );
  },
});

const fileFilterDocument = (req, file, cb) => {
  if (
    file.mimetype !== 'application/pdf' ||
    file.mimetype !== 'application/msword'
  ) {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

const fileFilterSignImage = (req, file, cb) => {
  if (
    file.mimetype !== 'image/jpeg' ||
    file.mimetype !== 'image/png' ||
    file.mimetype !== 'image/jpg'
  ) {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

const uploadDocument = multer({
  storage: storageDocument,
  fileFilter: fileFilterDocument,
});

const uploadSignImage = multer({
  storage: storageSignImage,
  fileFilter: fileFilterSignImage,
});

module.exports = {
  uploadDocument,
  uploadSignImage,
};

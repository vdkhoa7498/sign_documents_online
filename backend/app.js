//3rd party library
const express = require("express");
const bodyParser = require("body-parser");
const morgan = require("morgan");
const cors = require("cors");
const mongoose = require("mongoose");
const httpStatus = require("http-status");
const passport = require("passport");

const ApiError = require("./utils/ApiError");
const { errorConverter, errorHandler } = require("./middlewares/error.mdw");

const app = express();

const PORT = process.env.PORT || 4000;

//Middlewares
app.use(bodyParser.json());
app.use(morgan("dev"));
app.use(cors());
app.use(passport.initialize());
app.use(passport.session());

require("./middlewares/routes.mdw")(app);

// send back a 404 error for any unknown api request
app.use((req, res, next) => {
  next(new ApiError(httpStatus.NOT_FOUND, "Route not found"));
});

// convert error to ApiError, if needed
app.use(errorConverter);

// handle error
app.use(errorHandler);

mongoose
  .connect(
    `mongodb+srv://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@cluster0.i3z9u.mongodb.net/${process.env.MONGO_DEFAULT_DATABASE}?retryWrites=true&w=majority`,
    { useNewUrlParser: true, useUnifiedTopology: true }
  )
  .then(() => {
    console.log("Connect Mongodb successfully");
    app.listen(PORT, () => {
      console.log(`Server is running on PORT ${PORT}`);
    });
  })
  .catch((error) => console.log(error));

const mongoose = require('mongoose');
const validator = require('validator');
const bcrypt = require('bcryptjs');

const { roles } = require('../config/roles.config');
const { Schema } = mongoose;

const userSchema = mongoose.Schema(
  {
    // Tên người dùng
    name: {
      type: String,
      required: true,
      trim: true,
    },
    // Email người dùng
    email: {
      type: String,
      required: true,
      trim: true,
      unique: true,
      lowercase: true,
      validate(value) {
        if (!validator.isEmail(value)) {
          throw new Error('Invalid email');
        }
      },
    },
    // Mật khẩu người dùng
    password: {
      type: String,
      required: true,
      trim: true,
      minlength: 8,
      validate(value) {
        if (!value.match(/\d/) || !value.match(/[a-zA-Z]/)) {
          throw new Error(
            'Password must contain at least one letter and one number'
          );
        }
      },
    },
    // Trường phân biệt Admin hoặc User
    role: {
      type: String,
      enum: roles,
      default: 'user',
    },
    // Avatar người dùng
    avatar: {
      type: String,
      trim: true,
    },
    // Đường dẫn chữ ký người dùng
    signImageUrl: {
      type: String,
      trim: true,
    },
    // Đường dẫn tên viết tắt người dùng
    initialsUrl: {
      type: String,
      trim: true,
    },
    // Thư mục inbox
    inboxes: {
      type: [{ type: Schema.Types.ObjectId, ref: 'Envelope' }],
      default: [],
    },
    // Thư mục sent
    sents: {
      type: [{ type: Schema.Types.ObjectId, ref: 'Envelope' }],
      default: [],
    },
    // Thư mục draft
    drafts: {
      type: [{ type: Schema.Types.ObjectId, ref: 'Envelope' }],
      default: [],
    },
    // Thư mục deleted
    deleteds: {
      type: [{ type: Schema.Types.ObjectId, ref: 'Envelope' }],
      default: [],
    },
  },
  { timestamps: true }
);

userSchema.statics.isEmailTaken = async function (email) {
  const user = await this.findOne({ email: email });
  return !!user;
};

userSchema.methods.isPasswordMatch = async function (password) {
  return bcrypt.compare(password, this.password);
};

const User = mongoose.model('User', userSchema);

module.exports = User;

const mongoose = require("mongoose");
const { Schema } = mongoose;

const envelopeSchema = mongoose.Schema(
  {
    // Envelope Id (uuid)
    // Dùng trường này để làm id cho envelope, không dùng _id
    envelopeId: {
      type: String,
      required: true,
    },
    // Người tạo và gửi envelope
    sender: {
      user: { type: Schema.Types.ObjectId, ref: "User" },
      // Trạng thái envelope của người gửi
      // Draft khi:
      // - Người dùng move envelope vào thư mục draft
      // - Người dùng trong giai đoạn tạo envelope, chưa tạo xong thì ấn thoát, bấm vào save and close
      // Completed khi:
      // - Các bên đã ký kết xong
      // Waiting khi:
      // - Các bên chưa ký kết xong
      // Voided khi:
      // - Người dùng bấm delete envelope
      // - Người dùng trong giai đoạn tạo envelope, chưa tạo xong thì ấn thoát, bấm vào discard change
      // Declined khi:
      // - Người nhận từ chối ký văn bản
      envelopeStatus: {
        type: String,
        required: true,
        enum: ["Draft", "Completed", "Waiting", "Voided", "Declined"],
        default: "Draft",
      },
    },
    // Danh sách người nhận
    recipients: [
      {
        // Reference đến model User
        user: { type: Schema.Types.ObjectId, ref: "User" },
        // Tên người nhận do người gửi đặt ở phần 2 add recipients
        name: {
          type: String,
        },
        // Loại người nhận (Needs to sign(Cần ký), In person signer(Người ký tên trực tiếp), Receive a copy(Người được nhận 1 bản copy sau khi ký kết xong))
        // Trước hết làm flow Needs to sign
        type: {
          type: String,
          default: "Needs to sign",
        },
        // Nếu type là Needs to sign thì có trường này
        // Trường cho biết xem người đó đã ký văn bản hay chưa
        didSign: {
          type: Boolean,
          default: false,
        },
        // Trạng thái envelope của người nhận
        // Completed khi:
        // - Người nhận này đã ký xong văn bản
        // Needs to sign khi:
        // - Người này chưa ký văn bản
        // Voided khi:
        // - Bên gửi voided envelope
        // Declined khi:
        // - Người nhận từ chối ký
        envelopeStatus: {
          type: String,
          required: true,
          enum: ["Completed", "Needs to sign", "Voided", "Declined"],
          default: "Needs to sign",
        },
      },
    ],
    // Tạm thời làm chỉ upload đc 1 file trước
    file: {
      // Tên file document
      name: {
        type: String,
      },
      // Đường dẫn file document
      url: {
        type: String,
      },
    },
  },
  { timestamps: true }
);

const Envelope = mongoose.model("Envelope", envelopeSchema);

module.exports = Envelope;

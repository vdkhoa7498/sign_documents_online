const User = require('./user.model');
const Envelope = require('./envelope.model');

module.exports = {
  User,
  Envelope,
};

const { authRoute, envelopeRoute, userRoute } = require('../routes');

module.exports = (app) => {
  app.use('/user', userRoute);
  app.use('/auth', authRoute);
  app.use('/envelope', envelopeRoute);
};

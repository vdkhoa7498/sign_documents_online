const httpStatus = require('http-status');

const catchAsync = require('../utils/catchAsync');
const { userService } = require('../services');

const addSignImageUrl = catchAsync(async (req, res) => {
  const userId = req.params.userId;
  const user = await userService.getUserById(userId);
  const updatedUser = await userService.addSignImageUrl(
    user,
    req.file.path
  );
  res.status(httpStatus.OK).send({ success: true, updatedUser });
});

module.exports = {
  addSignImageUrl,
};

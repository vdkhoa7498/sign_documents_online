const httpStatus = require("http-status");

const catchAsync = require("../utils/catchAsync");
const { envelopeService, userService } = require("../services");

const getEnvelopeByEnvelopeId = catchAsync(async (req, res) => {
  const { envelopeId } = req.params;
  const envelope = await envelopeService.getEnvelopeByEnvelopeId(envelopeId);
  res.status(httpStatus.CREATED).send({ success: true, envelope });
});

const createEmptyEnvelope = catchAsync(async (req, res) => {
  const { senderId } = req.body;
  console.log(req.body);
  const envelope = await envelopeService.createEmptyEnvelope(senderId);
  res.status(httpStatus.CREATED).send({ success: true, envelope });
});

const addDocumentToEnvelope = catchAsync(async (req, res) => {
  const { envelopeId } = req.params;
  const envelope = await envelopeService.addDocumentToEnvelope(
    req.file,
    envelopeId
  );
  res.status(httpStatus.CREATED).send({ success: true, envelope });
});

const addRecipientsToEnvelope = catchAsync(async (req, res) => {
  const { envelopeId } = req.params;
  const envelope = await envelopeService.getEnvelopeByEnvelopeId(envelopeId);
  const updatedEnvelope = await envelopeService.addRecipientsToEnvelope(
    envelope,
    req.body
  );
  res.status(httpStatus.OK).send({ success: true, updatedEnvelope });
});

const deleteEnvelope = catchAsync(async (req, res) => {
  const envelopeId = req.params.envelopeId;
  const deletedEnvelope = await envelopeService.deleteEnvelope(envelopeId);
  res.status(httpStatus.OK).send({ success: true, deletedEnvelope });
});

const downloadEnvelope = catchAsync(async (req, res) => {
  const envelopeId = req.params.envelopeId;
  const { file, extension, filename } = await envelopeService.downloadEnvelope(
    envelopeId
  );
  res.setHeader("Content-Type", `application/${extension}`);
  res.setHeader("Content-Disposition", `inline; filename="${filename}"`);
  file.pipe(res);
});

const getInboxes = catchAsync(async (req, res) => {
  const userId = req.query.userId;
  // const user = await userService.findInboxes(userId);
  // res.status(httpStatus.OK).send({ success: true, inboxes: user.inboxes });
  const user = await userService.findInboxes(userId);
  console.log("user : ", user);
  console.log("inboxes : ", user.inboxEnvelopes);
  res.status(httpStatus.OK).send({ success: true, inbox: user.inboxEnvelopes });
});

const getSents = catchAsync(async (req, res) => {
  const userId = req.query.userId;
  const user = await userService.findSents(userId);
  res.status(httpStatus.OK).send({ success: true, sents: user.sents });
});

const getDrafts = catchAsync(async (req, res) => {
  const userId = req.query.userId;
  const user = await userService.findDrafts(userId);
  res.status(httpStatus.OK).send({ success: true, drafts: user.drafts });
});

const getDeleteds = catchAsync(async (req, res) => {
  const userId = req.query.userId;
  const user = await userService.findDeleteds(userId);
  res.status(httpStatus.OK).send({ success: true, deleteds: user.deleteds });
});

module.exports = {
  getEnvelopeByEnvelopeId,
  createEmptyEnvelope,
  addDocumentToEnvelope,
  addRecipientsToEnvelope,
  deleteEnvelope,
  downloadEnvelope,
  getInboxes,
  getSents,
  getDrafts,
  getDeleteds,
};

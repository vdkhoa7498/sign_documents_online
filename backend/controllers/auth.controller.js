const httpStatus = require('http-status');

const catchAsync = require('../utils/catchAsync');
const { authService, userService, tokenService } = require('../services');

const register = catchAsync(async (req, res) => {
  const user = await userService.createUser(req.body);
  res.status(httpStatus.CREATED).json({ success: true, userId: user._id });
});

const login = catchAsync(async (req, res) => {
  const { email, password } = req.body;
  const user = await authService.loginUserWithEmailAndPassword(email, password);
  const token = await tokenService.generateAuthToken(user);
  res.status(httpStatus.OK).json({
    success: true,
    accessToken: token,
    user: {
      _id: user._id.toString(),
      name: user.name,
      email: user.email,
      signImageUrl: user.signImageUrl,
    },
  });
});

// Handler for OAuth Google, Facebook
const authExternal = async (req, res, next) => {
  console.log('Login OAuth');
  const { user } = req;
  const { id } = user;
  const type = user.provider.toUpperCase();
  let userModel;

  const userInfo = await authenticateExternal({ id, type });
  if (!userInfo) {
    userModel = await createUserExternal(user, type);
    console.log('user dang nhap lan dau: ', userModel._id);
  } else {
    userModel = await User.findOne({ email: user.email }).exec();
    console.log('user dang nhap roi: ', userModel._id);
  }
  const token = JwtUtils.createAuthToken(userModel);
  const userId = userModel._id.toString();

  res.redirect(`http://localhost:3000/saveToken/${token}/${userId}`);
};

const authenticateExternal = async ({ id, type }) => {
  let instance;
  switch (type) {
    case 'GOOGLE':
      instance = await User.findOne({ googleId: id }).exec();
      if (instance) {
        return instance;
      }
      break;
  }
  return null;
};

const createUserExternal = async (userInfo, type) => {
  const { id, displayName, email } = userInfo;
  let user;
  let userModel;
  switch (type) {
    case 'GOOGLE':
      userModel = new User({
        fullName: displayName,
        email: email,
        googleId: id,
      });
      user = await userModel.save();
      break;
  }
  return user;
};

module.exports = {
  register,
  login,
  authExternal,
};
